import model.*;

public class Application {
    public static void main(String[] args) {
        Kotik kotikOne = new Kotik(2, "vasya", 9, "myafk");

        Kotik kotikTwo = new Kotik();
        kotikTwo.setKotik(5, "persik", 5, "meeeow");

        kotikOne.liveAnotherDay();
        System.out.println();
        System.out.println("name: " + kotikOne.getName());
        System.out.println("weight: " + kotikOne.getWeight());

        System.out.println();
        System.out.println();

        Boolean isMeowEquals = new String(kotikOne.getMeow()).equals(kotikTwo.getMeow());
        System.out.println("meow compare: " + isMeowEquals);
        System.out.println("cats: " + Kotik.getNumOfClassInstances());
    }
}
