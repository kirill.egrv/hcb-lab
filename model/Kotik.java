package model;

import java.util.Random;

/**
 * Kotik
 */
public class Kotik {
    static int numOfClassInstances = 0;
    int satiety = 10;

    int weight = -1;
    int prettiness = -1;
    String name = "";
    String meow = "";

    static String[] methods = { "play", "sleep", "catchMouse", "sneeze", "yawn" };

    public Kotik() {
        this.init();
    }

    public Kotik(int prettiness, String name, int weight, String meow) {
        this.init();
        this.setKotik(prettiness, name, weight, meow);
    }

    void init() {
        numOfClassInstances++;
    }

    public void setKotik(int prettiness, String name, int weight, String meow) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
    }

    /**
     * 
     * @param ncus - number of conditional units of satiety
     */
    Boolean eat(int ncus) {
        if (ncus > 0) {
            this.incSatietyByNum(ncus);
            // System.out.println("🐈: i'm eating");
            return true;
        }

        // System.out.println("🐈: fffr");
        return false;
    }

    /**
     * 
     * @param ncus
     * @param foodName = {whiskas, milk, mouse}
     * @return
     */
    Boolean eat(int ncus, String foodName) {
        // System.out.println("🐈: urrr, " + foodName);

        return this.eat(ncus);
    }

    Boolean eat() {
        return this.eat(10, "mouse");
    }

    Boolean play() {
        if (this.checkSatiety()) {
            System.out.println("🐈: i'm playing");
            this.decSatietyByNum(5);
            return true;
        }

        return false;
    }

    Boolean sleep() {
        if (this.checkSatiety()) {
            System.out.println("🐈: i'm sleeping");
            this.decSatietyByNum(3);
            return true;
        }

        return false;
    }

    Boolean catchMouse() {
        if (this.checkSatiety()) {
            System.out.println("🐈: i'm catching mouse");
            this.decSatietyByNum(7);
            this.eat();
            return true;
        }

        return false;
    }

    Boolean sneeze() {
        if (this.checkSatiety()) {
            System.out.println("🐈: i'm sneezing");
            return true;
        }

        return false;
    }

    Boolean yawn() {
        if (this.checkSatiety()) {
            System.out.println("🐈: i'm yawning");
            return true;
        }

        return false;
    }

    public void liveAnotherDay() {
        for (int i = 1; i <= 24; i++) {
            if (!invokeRandomMethod()) {
                this.eat();
            }
        }
    }

    Boolean invokeRandomMethod() {
        String m = getRandomMethod();
        return invokeMethod(m);
    }

    String getRandomMethod() {
        Random r = new Random();
        int randomIndex = r.nextInt(methods.length);
        return methods[randomIndex];
    }

    Boolean invokeMethod(String methodName) {
        switch (methodName) {
        case "play":
            return this.play();
        case "sleep":
            return this.sleep();
        case "catchMouse":
            return this.catchMouse();
        case "sneeze":
            return this.sneeze();
        case "yawn":
            return this.yawn();
        default:
            System.out.println("no method " + methodName);
            return false;
        }
    }

    Boolean checkSatiety() {
        if (this.getSatiety() <= 0) {
            System.out.println("🐈: give me food!");
            return false;
        }
        return true;
    }

    public void incSatietyByNum(int num) {
        this.satiety += num;
    }

    public void decSatietyByNum(int num) {
        this.satiety -= num;
    }

    public int getSatiety() {
        return satiety;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public String getMeow() {
        return meow;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public static int getNumOfClassInstances() {
        return numOfClassInstances;
    }
}